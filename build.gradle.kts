group "org.projects.sites"
version "1.0-SNAPSHOT"

plugins {
    id("org.jetbrains.kotlin.js") version "1.4-M3"
}

allprojects {
    repositories {
        maven("https://dl.bintray.com/kotlin/kotlin-eap")
        jcenter()
        mavenCentral()
    }
}
subprojects { //TODO search buildscript
    apply(plugin =  "org.jetbrains.kotlin.js")
    buildscript {    }
}

repositories {
    maven("https://dl.bintray.com/kotlin/kotlin-eap")
    jcenter()
    mavenCentral()
}