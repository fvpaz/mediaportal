version = "0.0.5"

repositories {

}

dependencies {
    implementation(kotlin("stdlib-js"))
}

kotlin {
    js {
        browser { }
        binaries.executable()
    }
}