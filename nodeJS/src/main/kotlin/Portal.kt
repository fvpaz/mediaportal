import kotlinx.coroutines.*
import kotlin.js.Promise
import db.mongo.*
external fun require(module: String): dynamic

fun foo() {
    val ejsArgs: dynamic = object {}
    ejsArgs["title"] = "Play in Portal!"

    val router = require("koa-router")()

    router.get("/index2") { ctx, next ->
        GlobalScope.promise {
            val renderPromise: Promise<Any> = ctx.render("index", ejsArgs)
            connection()
            renderPromise.await()
        }
    }

    return router
}