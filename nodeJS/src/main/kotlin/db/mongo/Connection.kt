package db.mongo

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.await
import kotlinx.coroutines.promise
import kotlin.js.Promise

external fun require(pkg:String):dynamic


val MongoClient = require("mongodb").MongoClient;
val assert = require("assert");

fun connection(){
    GlobalScope.promise {
        // Connection URL
        val url = "mongodb://localhost:27017"
        // Database Name
        val dbName = ""

        val mongoArgs: dynamic = object {}

        mongoArgs["useNewUrlParser"] =  true
        val client = MongoClient(url, mongoArgs)

        try {
            // Use connect method to connect to the Server
            val clientPromise: Promise<Any> = client.connect()
            clientPromise.await()

            console.log("Connected correctly to server")
            val db = client.db(dbName)
            console.log("Connected to $dbName")
        } catch (err: dynamic) {
            console.log(err.stack);
        }

        client.close();
    }
}
//(async function() {
//
//})();