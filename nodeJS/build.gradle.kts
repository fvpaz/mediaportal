version = "0.0.3"

dependencies {
    implementation(kotlin("stdlib-js"))
    implementation(npm("cookie-parser", "^1.4.5"))
    implementation(npm("debug", "^4.1.1"))
    implementation(npm("koa", "^2.7.0"))
    implementation(npm("koa-hbs", "^0.9.0"))
    implementation(npm("http-errors", "^1.7.3"))
//    implementation(npm("kotlin", "^1.4.0-M2")) //https://yarnpkg.com/package/kotlin
    implementation(npm("morgan", "^1.10.0"))
    implementation(npm("node-sass-middleware", "0.11.0"))
    implementation(npm("pg-promise", "^10.5.5"))
    implementation(npm("koa-bodyparser", "^4.2.1"))
    implementation(npm("koa-convert", "^1.2.0"))
    implementation(npm("koa-json", "^2.0.2"))
    implementation(npm("koa-logger", "^3.2.0"))
    implementation(npm("koa-onerror", "^4.1.0"))
    implementation(npm("koa-router", "^7.4.0"))
    implementation(npm("koa-static", "^5.0.0"))
    implementation(npm("koa-views", "^6.2.0"))
    implementation(npm("mongodb", "^3.5.8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:1.3.7")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-common:1.3.7")
}

kotlin {
    js {
        nodejs { }
        binaries.executable()
    }
}

//Work call to :compileKotlinJs
tasks {
//    named("compileKotlinJs", org.jetbrains.kotlin.gradle.dsl.KotlinCompile::class){
//        kotlinOptions.outputFile = "${projectDir}/routes/node/index2.js"
//        kotlinOptions.metaInfo = false
//        kotlinOptions.moduleKind = "commonjs"
//    }

    named("compileKotlinJs", org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile::class) {
        kotlinOptions.outputFile = "${projectDir}/routes/node/index2.js"
        kotlinOptions.metaInfo = false
        kotlinOptions.moduleKind = "commonjs"
    }
}

//tasks.register("syncCompileKotlinJs", Copy::class.java) {
//    val compile = tasks.withType(org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile::class.java).named("compileKotlinJs")
//    into("routes/node") {
//        from(
//            compile
//                .map {
//                    it.outputFile.parentFile
//                }
//        )
//    }
//    into("$buildDir")
//    dependsOn(compile)
//}