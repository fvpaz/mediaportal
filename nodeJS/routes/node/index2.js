(function (_, Kotlin, $module$kotlinx_coroutines_core) {
  'use strict';
  var coroutines = $module$kotlinx_coroutines_core.kotlinx.coroutines;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var await_0 = $module$kotlinx_coroutines_core.kotlinx.coroutines.await_t11jrl$;
  var COROUTINE_SUSPENDED = Kotlin.kotlin.coroutines.intrinsics.COROUTINE_SUSPENDED;
  var CoroutineImpl = Kotlin.kotlin.coroutines.CoroutineImpl;
  var promise = $module$kotlinx_coroutines_core.kotlinx.coroutines.promise_pda6u4$;
  var MongoClient;
  var assert;
  function connection$lambda$ObjectLiteral() {
  }
  connection$lambda$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: []
  };
  function Coroutine$connection$lambda($receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 4;
    this.local$dbName = void 0;
    this.local$client = void 0;
  }
  Coroutine$connection$lambda.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$connection$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$connection$lambda.prototype.constructor = Coroutine$connection$lambda;
  Coroutine$connection$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            var url = 'mongodb://localhost:27017';
            this.local$dbName = 'stars';
            var mongoArgs = new connection$lambda$ObjectLiteral();
            mongoArgs['useNewUrlParser'] = true;
            this.local$client = MongoClient(url, mongoArgs);
            this.exceptionState_0 = 2;
            var clientPromise = this.local$client.connect();
            this.state_0 = 1;
            this.result_0 = await_0(clientPromise, this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            console.log('Connected correctly to server');
            var db = this.local$client.db(this.local$dbName);
            console.log('Connected to stars');
            this.exceptionState_0 = 4;
            this.state_0 = 3;
            continue;
          case 2:
            this.exceptionState_0 = 4;
            var err = this.exception_0;
            console.log(err.stack);
            this.state_0 = 3;
            continue;
          case 3:
            return this.local$client.close();
          case 4:
            throw this.exception_0;
          default:this.state_0 = 4;
            throw new Error('State Machine Unreachable execution');
        }
      } catch (e) {
        if (this.state_0 === 4) {
          this.exceptionState_0 = this.state_0;
          throw e;
        } else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function connection$lambda($receiver_0, continuation_0, suspended) {
    var instance = new Coroutine$connection$lambda($receiver_0, this, continuation_0);
    if (suspended)
      return instance;
    else
      return instance.doResume(null);
  }
  function connection() {
    promise(coroutines.GlobalScope, void 0, void 0, connection$lambda);
  }
  function foo$ObjectLiteral() {
  }
  foo$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: []
  };
  function Coroutine$foo$lambda$lambda(closure$ctx_0, closure$ejsArgs_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$ctx = closure$ctx_0;
    this.local$closure$ejsArgs = closure$ejsArgs_0;
  }
  Coroutine$foo$lambda$lambda.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$foo$lambda$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$foo$lambda$lambda.prototype.constructor = Coroutine$foo$lambda$lambda;
  Coroutine$foo$lambda$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            var renderPromise = this.local$closure$ctx.render('index', this.local$closure$ejsArgs);
            connection();
            this.state_0 = 2;
            this.result_0 = await_0(renderPromise, this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            return this.result_0;
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      } catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        } else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function foo$lambda$lambda(closure$ctx_0, closure$ejsArgs_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$foo$lambda$lambda(closure$ctx_0, closure$ejsArgs_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function foo$lambda(closure$ejsArgs) {
    return function (ctx, next) {
      return promise(coroutines.GlobalScope, void 0, void 0, foo$lambda$lambda(ctx, closure$ejsArgs));
    };
  }
  function foo() {
    var ejsArgs = new foo$ObjectLiteral();
    ejsArgs['title'] = 'Play in Portal!';
    var router = require('koa-router')();
    router.get('/index2', foo$lambda(ejsArgs));
    return router;
  }
  var package$db = _.db || (_.db = {});
  var package$mongo = package$db.mongo || (package$db.mongo = {});
  Object.defineProperty(package$mongo, 'MongoClient', {
    get: function () {
      return MongoClient;
    }
  });
  Object.defineProperty(package$mongo, 'assert', {
    get: function () {
      return assert;
    }
  });
  package$mongo.connection = connection;
  _.foo = foo;
  MongoClient = require('mongodb').MongoClient;
  assert = require('assert');
  Kotlin.defineModule('index2', _);
  return _;
}(module.exports, require('kotlin'), require('kotlinx-coroutines-core')));

//# sourceMappingURL=index2.js.map
