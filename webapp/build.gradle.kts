
version = "0.0.1"

repositories {
    maven("https://kotlin.bintray.com/kotlin-js-wrappers/")
}
val kotlinVersion   = "1.4-M3"
val buildKotlinlibs = "pre.109-kotlin"
val reactVersion    = "16.13.1"
val reactKotlin     = "$reactVersion-$buildKotlinlibs-$kotlinVersion"
val styleVersion    = "1.0.0-$buildKotlinlibs-$kotlinVersion"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-js")
    testImplementation("org.jetbrains.kotlin:kotlin-test-js")

    //React, React DOM + Wrappers
    implementation("org.jetbrains.kotlinx:kotlinx-html-js:0.7.1")
    implementation("org.jetbrains:kotlin-react:$reactKotlin") //https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-react
    implementation("org.jetbrains:kotlin-react-dom:$reactKotlin")
    implementation(npm("react", "$reactVersion"))
    implementation(npm("react-dom", "$reactVersion"))

    //Kotlin Styled (CSS)
    implementation("org.jetbrains:kotlin-styled:$styleVersion")  //https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-styled
//    implementation(npm("styled-components"))
//    implementation(npm("inline-style-prefixer"))
}

kotlin {
    js {
        browser { }
        binaries.executable()
    }
}